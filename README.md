# Tasks api

## Install locally

- Make sure to have Nodejs 20 installed
- install modules

```
npm install
```

- Create .env file in this api directory, see .env-sample

- Start the project

```
npm start
```

## Remote url

https://ecarrillo-task-api.vercel.app/api
