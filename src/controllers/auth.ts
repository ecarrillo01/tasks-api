import { Request, Response, NextFunction } from 'express';
import jwt, { JsonWebTokenError } from 'jsonwebtoken';
import UserModel from '../models/User';
import { compareSync, hashSync } from 'bcryptjs';
import mongoose from 'mongoose';
import validator from 'validator';
import AuthTokenModel from '../models/AuthToken';
import { registerSchema } from '../lib/validators/validators';

type Token = {
  email: string;
  id: mongoose.Types.ObjectId;
};

const JWT_SECRET = process.env.JWT_SECRET ?? 'b82d3eb4e9703b82dcd3ede4ae4b04dd';
const JWT_EXPIRES_IN = process.env.JWT_EXPIRES_IN ?? '90d';

const hashPassword = (password: string) => hashSync(password, 8);

const signToken = ({ id, email }: Token) =>
  jwt.sign({ id, email }, JWT_SECRET, {
    expiresIn: JWT_EXPIRES_IN,
  });

const validateLoginFields = (req: Request) => {
  if (!req.body?.email) {
    return 'Please provide your email';
  }
  if (!validator.isEmail(req.body.email)) {
    return `invalid email address ${req.body.email}`;
  }
  if (!req.body?.password) {
    return 'Please provide your password';
  }
  return null;
};

const createToken = async (data: Token): Promise<string> => {
  const token = signToken(data);
  await AuthTokenModel.create({
    user_id: data.id,
    token,
  });
  return token;
};

export async function signUp(req: Request, res: Response, next: NextFunction) {
  try {
    const validationErr = registerSchema.safeParse(req.body);
    if (!validationErr.success) {
      return res
        .status(400)
        .json({ message: validationErr.error.issues[0].message });
    }

    const foundUser = await UserModel.findOne({ email: req.body.email }).lean();
    if (foundUser) {
      return res
        .status(400)
        .json({ message: `Email ${req.body.email} already exists` });
    }
    const newUser = await UserModel.create({
      name: req.body.name,
      email: req.body.email,
      password: hashPassword(req.body.password),
    });
    const token = await createToken({ id: newUser.id, email: newUser.email });
    return res.status(201).json({ token });
  } catch (e) {
    return next(e);
  }
}

export async function login(req: Request, res: Response, next: NextFunction) {
  try {
    const validationErr = validateLoginFields(req);
    if (validationErr) {
      return res.status(400).json({ message: validationErr });
    }
    const { email, password } = req.body;
    const user = await UserModel.findOne({ email }).select('+password').lean();
    if (!user || !compareSync(password, user.password)) {
      return res.status(401).json({
        message: 'Invalid username or password',
      });
    }
    const token = await createToken({ id: user._id, email: user.email });
    return res.status(200).json({
      token,
    });
  } catch (e) {
    return next(e);
  }
}

export async function logOut(req: Request, res: Response, next: NextFunction) {
  try {
    if (!req.user?.id) return res.status(401).json({ message: 'Invalid user' });
    const token = req.headers.authorization?.split(' ')[1];
    if (!token) return res.status(400).json({ message: 'Missing token' });
    const foundToken = await AuthTokenModel.findOneAndDelete({
      token,
      user_id: req.user.id,
    });

    if (!foundToken) {
      return res.status(404).json({ message: 'Session not found' });
    }
    delete req.user;
    return res.json({ message: 'sign out succeeded' });
  } catch (e) {
    console.log(e);
    return res
      .status(500)
      .json({ message: 'There was an error trying to sign out' });
  }
}

export async function ensureAuthenticated(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    if (!req.headers.authorization) {
      return res.status(400).json({ message: 'Missing authorization header' });
    }

    if (!req.headers.authorization.startsWith('Bearer')) {
      return res
        .status(400)
        .json({ message: 'Authorization header must be a Bearer token' });
    }

    const token = req.headers.authorization.split(' ')[1];
    const payload = jwt.verify(token, JWT_SECRET) as Token;
    const tokenSession = await AuthTokenModel.findOne({
      user_id: payload.id,
      token,
    })
      .populate('user_id')
      .lean()
      .exec();

    if (!tokenSession?.user_id) {
      delete req.user;
      return res.status(401).json({ message: 'Unauthorized' });
    }
    req.user = {
      id: payload.id,
      email: payload.email,
    };
    return next();
  } catch (e) {
    if (e instanceof JsonWebTokenError) {
      return res.status(403).json({ message: 'Invalid access token' });
    }
    return next(e);
  }
}
