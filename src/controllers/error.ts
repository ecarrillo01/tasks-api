import { Request, Response ,NextFunction} from "express";

export default function errorController(e: unknown, _: Request, res: Response,next:NextFunction) {
    console.log(e);
    if (e instanceof Error) {
        return res.status(500).json({ message: e.message });
    }

    return res.status(500).json({ message: 'Something whent wrong' });
}