import { Request, Response, NextFunction } from 'express';
import TaskModel from '../models/Task';
import { TaskValidator } from '../lib/validators/validators';
import { ZodError } from 'zod';

export async function getOneTask(req: Request, res: Response) {
  try {
    const taskId = req.params.id;
    const doc = await TaskModel.findOne()
      .where({ user: req.user?.id, _id: taskId })
      .lean();
    return res.json(doc);
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message:
        'Something whent wrong, please verify that the provided task id is a valid ObjectId',
    });
  }
}

export async function getTasks(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const doc = await TaskModel.find()
      .where({ user: req.user?.id })
      .select('-__v -createdAt -updatedAt');
    return res.json(doc);
  } catch (e) {
    return next(e);
  }
}

export async function createTask(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const validated = TaskValidator.parse({
      title: req.body.title,
      description: req.body.description,
      dueDate: new Date(req.body.dueDate),
    });

    const doc = await TaskModel.create({
      title: validated.title,
      description: validated.description,
      dueDate: validated.dueDate,
      user: req.user?.id,
    });
    return res.status(201).json(doc);
  } catch (e) {
    if (e instanceof ZodError) {
      console.log(e.message);
      return res.status(400).json({ message: JSON.parse(e.message) });
    }
    return next(e);
  }
}

export async function updateTask(req: Request, res: Response) {
  try {
    if (!req.user?.id) return res.status(401).json({ message: 'Invalid user' });
    const taskId = req.params.id;
    const doc = await TaskModel.findOneAndUpdate(
      { _id: taskId, user: req.user.id },
      {
        title: req.body.title,
        description: req.body.description,
        dueDate: req.body.dueDate ? new Date(req.body.dueDate) : undefined,
      },
      {
        new: true,
        runValidators: true,
      }
    );
    if (!doc) {
      return res.status(404).json({ message: `Task ${taskId} not found` });
    }
    return res.json(doc);
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message:
        'Something whent wrong, please verify that the provided task id is a valid ObjectId',
    });
  }
}

export async function deleteTask(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    if (!req.user?.id) return res.status(401).json({ message: 'Invalid user' });
    const taskId = req.params.id;
    const doc = await TaskModel.findOneAndDelete({
      _id: taskId,
      user: req.user.id,
    });
    if (!doc) {
      return res
        .status(404)
        .json({ message: 'No document found with id ' + req.params.id });
    }
    return res.json({ message: 'Deleted' });
  } catch (e) {
    return next(e);
  }
}
