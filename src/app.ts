import express, { Request, Response } from 'express';
import { rateLimit } from 'express-rate-limit';
import mongoose from 'mongoose';
import cors from 'cors';
import authRouter from './routes/auth';
import taskRouter from './routes/task';
import errorController from './controllers/error';

const app = express();
const port = process.env.PORT ?? 3000;

const limiter = rateLimit({
  max: 200,
  windowMs: 60 * 60 * 1000,
  message: 'To many request from this IP, please try again in an hour',
});

app.use(cors());
app.use(express.json({ limit: '10kb' }));
app.use(express.urlencoded({ extended: true, limit: '10kb' }));
app.use('/api', limiter);
app.use('/api/auth', authRouter);
app.use('/api/tasks', taskRouter);
app.use('/api', (_: Request, res: Response) => {
  return res.json({ version: '1.0.0' });
});
app.use(errorController);

app.listen(port, () => {
  console.log(`App running at http://localhost:${port}/api`);
});

if (process.env.DATABASE_URL) {
  mongoose.connect(process.env.DATABASE_URL);
} else {
  console.log('Missing env variable DATABASE_URL');
  process.exit();
}

export default app;
