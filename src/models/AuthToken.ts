import { Schema, model } from 'mongoose';

const schema = new Schema(
  {
    token: {
      unique: true,
      type: String,
      required: [true, 'Please provide a token'],
    },
    user_id: {
      type: Schema.Types.ObjectId,
      required: [true, 'Please provide a user id'],
      ref: 'users',
    },
  },
  { timestamps: true }
);

export default model('auth_tokens', schema);
