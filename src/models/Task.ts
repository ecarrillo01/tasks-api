import { InferSchemaType, Schema, model } from 'mongoose';

export type Task = InferSchemaType<typeof taskSchema>;

const taskSchema = new Schema(
  {
    title: {
      type: String,
      required: [true, 'Please provide a title'],
      maxLength: 60,
    },
    description: {
      type: String,
      required: [true, 'Please provide a description'],
      maxLength: 150,
    },
    dueDate: {
      type: Date,
      required: [true, 'Please provide a due date'],
    },
    user: {
      type: Schema.Types.ObjectId,
      required: [true, 'Please provide a user id'],
      ref: 'users',
    },
  },
  {
    timestamps: true,
    toJSON: {
      virtuals: true,
    },
    toObject: {
      virtuals: true,
    },
  }
);

taskSchema.virtual('id').get(function () {
  return this._id?.toHexString();
});

export default model('tasks', taskSchema);
