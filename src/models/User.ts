import { InferSchemaType, Schema, model } from 'mongoose';
import validator from 'validator';

export type User = InferSchemaType<typeof userSchema>;

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'Please provide your name'],
      maxLength: 64,
    },
    email: {
      type: String,
      required: [true, 'Please provide your email'],
      unique: true,
      lowercase: true,
      validate: [validator.isEmail, 'Please provide a valid email'],
      maxLength: 64,
    },
    password: {
      type: String,
      required: [true, 'Please provide a password'],
      minLength: 8,
      select: false,
      maxLength: 256,
    },
  },
  { timestamps: true }
);

export default model('users', userSchema);
