import type { User } from "../../models/User";

declare global {
  namespace Express {
    interface Request {
      user?: Pick<User, 'email' | 'id'>;
    }
  }
}