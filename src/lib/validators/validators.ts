import { isStrongPassword } from 'validator';
import { z } from 'zod';

export const TaskValidator = z.object({
  title: z.string().max(60, { message: 'Task title too long' }),
  description: z
    .string({ required_error: 'Please provide a description' })
    .max(150, { message: 'Task description too long' }),
  dueDate: z.date(),
});

export const registerSchema = z
  .object({
    name: z.string({ required_error: 'name is required' }),
    email: z
      .string({ required_error: 'email is required' })
      .email({ message: 'Invalid email address' }),
    password: z
      .string({
        required_error: 'Password must be at least 8 characters long.',
      })
      .min(8, { message: 'Password must be at least 8 characters long.' }),
    passwordConfirm: z.string({
      required_error: 'passwordConfirm is required',
    }),
  })
  .refine(({ password, passwordConfirm }) => password === passwordConfirm, {
    message: "Passwords don't match",
    path: ['passwordConfirm'],
  })
  .superRefine(({ password }, checkPassComplexity) => {
    if (!isStrongPassword(password)) {
      checkPassComplexity.addIssue({
        code: 'custom',
        path: ['password'],
        message: 'password does not meet complexity requirements',
      });
    }
  });
