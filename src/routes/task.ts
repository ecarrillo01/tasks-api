import express from 'express';
import { ensureAuthenticated } from '../controllers/auth';
import * as taskController from '../controllers/task';

const router = express.Router();

router.use(ensureAuthenticated);
router.get('/', taskController.getTasks);
router.get('/:id', taskController.getOneTask);
router.post('/', taskController.createTask);
router.patch('/:id', taskController.updateTask);
router.delete('/:id', taskController.deleteTask);

export default router;
