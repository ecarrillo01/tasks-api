import express from 'express';
import * as authController from '../controllers/auth';

const router = express.Router();

router.post('/signup', authController.signUp);
router.post('/login', authController.login);
router.delete(
  '/logout',
  authController.ensureAuthenticated,
  authController.logOut
);

export default router;
